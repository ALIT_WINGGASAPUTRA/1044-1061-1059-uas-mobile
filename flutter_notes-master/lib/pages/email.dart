import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';

void main() => runApp(Email());

class Email extends StatefulWidget{
  State<StatefulWidget> createState() => _Email();
}

class _Email extends State<Email>{
  final Completer<WebViewController> _webcontrol =
  Completer<WebViewController>();

  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar:AppBar(
          backgroundColor: Colors.green,
          title: Text('Email'),
        ),
        body: WebView(
          initialUrl: "https://www.google.com/intl/id/gmail/about/",
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webiewController){
            _webcontrol.complete(webiewController);
          },

        ),
      ),
    );
  }

}