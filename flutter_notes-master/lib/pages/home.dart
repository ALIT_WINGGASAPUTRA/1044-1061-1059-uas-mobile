import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_notes_app/models/note.dart';
import 'package:flutter_notes_app/pages/edit.dart';
import 'package:flutter_notes_app/pages/email.dart';
import 'package:flutter_notes_app/pages/instagram.dart';
import 'package:flutter_notes_app/service/db.dart';
import 'package:flutter_notes_app/widgets/loading.dart';
import 'package:flutter_notes_app/pages/about.dart';






class Home extends StatefulWidget {
		@override
		HomeState createState() => HomeState();
}


class HomeState extends State<Home> {

		List<Note> notes;
		bool loading = true;

		@override
		void initState() {
				super.initState();
				refresh();
		}

		@override
		Widget build(BuildContext context) {
				return Scaffold(
					drawer: SideDrawer(),
						appBar: AppBar(
							backgroundColor: Colors.indigoAccent,
								title: Text('Daily Schedule', style: TextStyle(fontSize: 27),),
						),
						floatingActionButton: FloatingActionButton(
								child: Icon(Icons.add),
								backgroundColor: Colors.indigoAccent,
								onPressed: () {
										setState(() => loading = true);
										Navigator.push(context, MaterialPageRoute(builder: (context) => Edit(note: new Note()))).then((v) {
												refresh();
										});
								},
						),
						body: loading? Loading() : ListView.builder(
								padding: EdgeInsets.all(5.0),
								itemCount: notes.length,
								itemBuilder: (context, index) {
										Note note = notes[index];
										return Card(
												color: Colors.white24,
												child: ListTile(
													title: Text(note.title,style: TextStyle(fontSize: 25)),
														leading: IconButton(
														icon: Icon(Icons.done),
															onPressed: () async {

															var result;
															if (result > 0) {
															print(result);
															Navigator.of(context).push(MaterialPageRoute(
															builder: (context) => Home()));

									}

									},



														),

														subtitle: Text(
																note.content,
																maxLines: 2,
																style: TextStyle(fontSize: 20),
																overflow: TextOverflow.ellipsis,
														),
														onTap: () {
																setState(() => loading = true);
																Navigator.push(context, MaterialPageRoute(builder: (context) => Edit(note: note))).then((v) {
																		refresh();
																});
														},
												),
										);
								},
						),
				);
		}


		Future<void> refresh() async {
				notes = await DB().getNotes();
				setState(() => loading = false);
		}

}
class SideDrawer extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Drawer(
			child: Column(
				children: <Widget>[
					DrawerHeader(
						child: Center(),
						decoration: BoxDecoration(
							borderRadius: BorderRadius.circular(0.0),
							image: DecorationImage(
								image: NetworkImage(
										'https://lh3.googleusercontent.com/-BDSXasEOJq0/YGKjO3Pe9CI/AAAAAAAAIiY/IUzA7GO1FlgT-nra7GwvDCTanbgml__uwCNcBGAsYHQ/pexels-daria-obymaha-1684149.jpg'),
								fit: BoxFit.cover,
							),
							color: Colors.deepPurpleAccent[400],
						),
					),
					ListTile(
						leading: Icon(Icons.home_outlined),
						title: Text('Home'),
						onTap: () => {
							Navigator.push(
									context,
						MaterialPageRoute(builder:(context) => Home()),
						),
						},
					),
					ListTile(
						leading: Icon(Icons.camera_alt_sharp),
						title: Text('Instagram'),
						onTap: () => {
							Navigator.push(
								context,
								MaterialPageRoute(builder:(context) => Instagram()),
							),
						},
					),
					ListTile(
						leading: Icon(Icons.attach_email_outlined),
						title: Text('Email'),
						onTap: () => {
							Navigator.push(
								context,
								MaterialPageRoute(builder:(context) => Email()),
							),
						},
					),
					ListTile(
						leading: Icon(Icons.info_outline),
						title: Text('About'),
						onTap: () => {
							Navigator.push(
								context,
								MaterialPageRoute(builder:(context) =>Developer()),
							),
						},
					),


				],
			),
		);
	}
}